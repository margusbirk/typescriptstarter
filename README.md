### What is this repository for? ###

minimal template for a typescript project

### Watch *.ts and run entrypoint on compilation ###

```
#!bash
cd typescriptstarter
npm run-script runtsc
```

## install additional TS dependencies ##


```
#!bash

npm i @types/jquery -S
```